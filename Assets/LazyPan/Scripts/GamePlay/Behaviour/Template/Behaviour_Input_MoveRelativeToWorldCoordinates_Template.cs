﻿using UnityEngine;

namespace LazyPan {
    public class Behaviour_Input_MoveRelativeToWorldCoordinates_Template : Behaviour {
        public Behaviour_Input_MoveRelativeToWorldCoordinates_Template(Entity entity, string behaviourSign) : base(entity, behaviourSign) {
        }

		/*获取移动物*/
		private void GetTarget() {
		}

		/*获取输入*/
		private void GetInput() {
		}

		/*获取速度*/
		private void GetSpeed() {
		}

		/*设置角色控制*/
		private void SetPlayerControl() {
		}

		/*获取可以移动*/
		private void GetPlayerControl() {
		}



        public override void Clear() {
            base.Clear();
        }
    }
}